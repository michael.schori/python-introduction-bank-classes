class Person:
    hair_color = 'Schwarz'

    def __init__(self, birthdate: int, eye_color: str, firstname: str, lastname: str):
        self.birthdate = birthdate
        self.eye_color = eye_color
        self.firstname = firstname
        self.lastname = lastname

    def walk(self):
        print(f'{self.firstname} {self.lastname} ist am laufen.')

    def speak(self, message: str):
        print(f'{self.firstname} {self.lastname} sagt: {message}')


# sarah = Person(2004, 'Grün', 'Sarah', 'Hergott')
# edwin = Person(2003, 'Braun', 'Edwin', 'Brünisholz')
# kevin = Person(2006, 'Blau', 'Kevin', 'Frey')
#
# print(sarah.eye_color, sarah.hair_color)
#
# kevin.hair_color = 'Braun'
# print(kevin.hair_color)
#
# edwin.walk()
# sarah.speak('Hallo zusammen')
