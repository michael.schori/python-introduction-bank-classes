from Person import Person
from Car import Car

sarah = Person(2004, 'Grün', 'Sarah', 'Hergott')
edwin = Person(2003, 'Braun', 'Edwin', 'Brünisholz')
kevin = Person(2006, 'Blau', 'Kevin', 'Frey')

print(sarah.eye_color, sarah.hair_color)

kevin.hair_color = 'Braun'
print(kevin.hair_color)

edwin.walk()
sarah.speak('Hallo zusammen')


car = Car('Mustang', 5)
car.add_driver(edwin)
car.start_engine()
car.stop_engine()
car.add_passenger(kevin)
car.add_passenger(sarah)

print(f'Aktuell sind {car.amount_passenger()} Beifahrer im Auto.')

car.remove_passenger(kevin)

print(f'Aktuell sind {car.amount_passenger()} Beifahrer im Auto.')

car.remove_driver()
