from Person import Person


class Car:
    driver = None
    passengeres = []
    running = False

    def __init__(self, brand: str, amount_seats: int):
        self.brand = brand
        self.amount_seats = amount_seats

    def amount_passenger(self):
        return len(self.passengeres)

    def add_driver(self, driver: Person):
        if not self.driver:
            self.driver = driver
        else:
            print('There is already a driver')

    def remove_driver(self):
        self.driver = None

    def add_passenger(self, passenger: Person):
        if self.amount_passenger() + 1 >= self.amount_seats:
            print('There are already too much passengers.')
        else:
            self.passengeres.append(passenger)

    def remove_passenger(self, passenger: Person):
        self.passengeres.remove(passenger)

    def start_engine(self):
        self.running = True
        print('Der Motor ist gestartet.')

    def stop_engine(self):
        self.running = False
        print('Der Motor ist gestoppt.')
