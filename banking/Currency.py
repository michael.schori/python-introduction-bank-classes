from enum import Enum

currency_rates = {
    'CHF': {
        'EUR': 0.92,
        'USD': 1.08
    },
    'EUR': {
        'CHF': 1.08,
        'USD': 1.17
    },
    'USD': {
        'CHF': 0.92,
        'EUR': 0.85
    }
}


class Currency(Enum):
    CHF = 'CHF'
    EUR = 'EUR'
    USD = 'USD'


class CurrencyExchanger:

    @staticmethod
    def exchange_currency(amount: float, from_currency: Currency, to_currency: Currency) -> float:
        return amount * currency_rates[f'{from_currency.name}'][f'{to_currency.name}']
