from BankAccount import BankAccount
from Money import Money
from Currency import Currency

bank = BankAccount('My Account', Currency.CHF)

bank.deposit(Money(20.5, Currency.CHF))
print(bank)

bank.deposit(Money(10, Currency.EUR))
print(bank)

bank.payout(Money(10, Currency.EUR))
print(bank)
