from Currency import Currency
from Money import Money


class BankAccount:

    def __init__(self, name: str, currency: Currency) -> None:
        self._name = name
        self._currency = currency
        self._balance = Money(0, self._currency)

    def __str__(self) -> str:
        return f'{self._name}: {self._balance}'

    @property
    def name(self) -> str:
        return self._name

    @property
    def balance(self) -> float:
        return self._balance.amount

    @property
    def currency(self) -> Currency:
        return self._currency

    def deposit(self, money: Money) -> None:
        self._balance + money

    def payout(self, money: Money) -> None:
        self._balance - money
