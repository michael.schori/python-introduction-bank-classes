from Currency import Currency, CurrencyExchanger


class Money:
    def __init__(self, amount: float, currency: Currency) -> None:
        self._amount = amount
        self._currency = currency

    def __str__(self) -> str:
        return f'{self._currency} {self._amount}'

    @property
    def amount(self) -> float:
        return self._amount

    @property
    def currency(self) -> Currency:
        return self._currency

    def __add__(self, other) -> None:
        if other.currency == self._currency:
            self._amount += other.amount
        else:
            self._amount += CurrencyExchanger.exchange_currency(other.amount, other.currency, self._currency)

    def __sub__(self, other) -> None:
        if other.currency == self._currency:
            self._amount -= other.amount
        else:
            self._amount -= CurrencyExchanger.exchange_currency(other.amount, other.currency, self._currency)
